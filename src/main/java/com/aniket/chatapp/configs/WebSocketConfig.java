package com.aniket.chatapp.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;


@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    //Web socket Configuration
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/sample-chat-app")
               .setAllowedOrigins( "http://127.0.0.1:5500","http://192.168.101.6:5500","http://elasticbeanstalk-us-east-2-590993679596.s3-website.us-east-2.amazonaws.com") //- in case your client and Server in different Domain due to CORS you need to add it
                .withSockJS(); //This application Prefix
        //SockJs for fallback safety
    }


    //Message Broker
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/topic/", "/queue/"); //Topic = PUB -SUB Model | /Queue - For one to One private Message
        registry.setApplicationDestinationPrefixes("/app"); //Prefix of URI Maps TO CONTROLLER - Simpler put - methods annotated by @MessageMapping will be triggered only if the message has one of the prefixes in the list
    }
}
