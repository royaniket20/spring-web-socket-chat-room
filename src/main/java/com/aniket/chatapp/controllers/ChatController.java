package com.aniket.chatapp.controllers;

import com.aniket.chatapp.models.ChatInMessage;
import com.aniket.chatapp.models.ChatOutMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@Slf4j
public class ChatController {

    /**
     * Instead of the annotation @SendTo, you can also use SimpMessagingTemplate which you can autowire inside your controller.
     * @MessageMapping("/news")
     * public void broadcastNews(@Payload String message) {
     *   this.simpMessagingTemplate.convertAndSend("/topic/news", message)
     * }
     * @param chatInMessage
     * @return
     */

    @GetMapping("/")
    public  String heathCheck(){
        log.info("System is up and Running : {}",new Date());
        return "success";
    }

    @MessageMapping("/guestChat") // Controller knows which URI it maps to
    @SendTo("/topic/guestChats") //Maps to message Queue or Topic
    public ChatOutMessage processMessage(ChatInMessage chatInMessage) {
        log.info("Processing the Message just came in : {}", chatInMessage);
        ChatOutMessage chatOutMessage = new ChatOutMessage();
        chatOutMessage.setContent(chatInMessage.getMessage());
        return chatOutMessage;
    }

    @MessageMapping("/guestName") // Controller knows which URI it maps to
    @SendTo("/topic/guestNames") //Maps to message Queue or Topic
    public ChatOutMessage processGuestJoined(ChatInMessage chatInMessage) {
        log.info("Processing the Guest Name just came in : {}", chatInMessage);
        ChatOutMessage chatOutMessage = new ChatOutMessage();
        chatOutMessage.setContent(chatInMessage.getMessage());
        return chatOutMessage;
    }


    @MessageMapping("/guestChatTyping") // Controller knows which URI it maps to
    @SendTo("/topic/guestChatsTypings") //Maps to message Queue or Topic
    public ChatOutMessage handleUserIsTyping(ChatInMessage chatInMessage) {
        log.info("Processing the Scenario that someone is trying : {}", chatInMessage);
        ChatOutMessage chatOutMessage = new ChatOutMessage();
        chatOutMessage.setContent("Someone is Typing ....");
        return chatOutMessage;
    }

    @MessageExceptionHandler // Message Error
    @SendTo("/topic/guestChatsErrors") //Maps to message Queue or Topic
    public ChatOutMessage handleGenericException(Throwable throwable) {
        log.info("Processing the Scenario that some Error Happened : {}", throwable.getMessage());
        ChatOutMessage chatOutMessage = new ChatOutMessage();
        chatOutMessage.setContent("An Error Happened ....");
        return chatOutMessage;
    }

}
