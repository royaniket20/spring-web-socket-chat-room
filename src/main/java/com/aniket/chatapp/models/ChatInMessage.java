package com.aniket.chatapp.models;

import lombok.Data;

import java.util.Date;


//Message Comes in from Browser

@Data
public class ChatInMessage {

    private String senderId;
    private String senderName;
    private String message;
    private Date timeStamp;


}
