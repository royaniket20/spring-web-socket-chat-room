package com.aniket.chatapp.models;

import lombok.Data;

import java.util.Date;


//Message Goes to Browser

@Data
public class ChatOutMessage {

    private String targetGroupName;
    private String content;
    private Date timeStamp;


}
