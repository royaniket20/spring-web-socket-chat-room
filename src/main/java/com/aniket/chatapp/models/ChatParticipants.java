package com.aniket.chatapp.models;

import lombok.Data;

import java.util.Date;


//Participants of the Chat communication

@Data
public class ChatParticipants {

    private String firstName;
    private String lastName;
    private String shortName;
    private String participantType;


}
